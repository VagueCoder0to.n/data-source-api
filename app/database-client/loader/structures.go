package loader

import "log"

// Loader is the Logger object for Loader
type Loader struct {
	*log.Logger
}

// SeriesList is a collection of series
type SeriesList []*Series

// Series is collection of data of each series
type Series struct {
	PosterLink  string    `json:"poster_url" bson:"poster_url"`
	Title       string    `json:"title" bson:"title"`
	Year        string    `json:"runtime_year" bson:"runtime_year"`
	Certificate string    `json:"certificate" bson:"certificate"`
	Runtime     int       `json:"runtime_of_episodes" bson:"runtime_of_episodes"`
	Genre       string    `json:"genre" bson:"genre"`
	Rating      float32   `json:"rating" bson:"rating"`
	Summary     string    `json:"summary" bson:"summary"`
	Cast        *CastList `json:"cast" bson:"cast"`
	Votes       int       `json:"no_of_votes" bson:"no_of_votes"`
}

// CastList is list of lead cast names in series
type CastList struct {
	Star1 string `json:"star1" bson:"star1"`
	Star2 string `json:"star2" bson:"star2"`
	Star3 string `json:"star3" bson:"star3"`
	Star4 string `json:"star4" bson:"star4"`
}
