package loader

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
)

// NewLoader initializes Loader object
func NewLoader(l *log.Logger) *Loader {
	return &Loader{l}
}

// NewSeriesList initializes SeriesList object
func (*Loader) NewSeriesList() *SeriesList {
	return &SeriesList{}
}

// NewSeries initializes SeriesList object
func (*Loader) NewSeries() *Series {
	return &Series{}
}

// ReadData reads data from CSV and returns reference to SeriesList
func (loader *Loader) ReadData() *SeriesList {
	file, err := os.OpenFile("./app/database-client/loader/imdb_series_dataset.csv", os.O_RDONLY, 444)
	if err != nil {
		loader.Fatal(err)
	}
	reader := csv.NewReader(file)
	data, err := reader.ReadAll()

	serieslist := loader.NewSeriesList()
	for i := 1; i < len(data); i++ {

		runtime, err := strconv.Atoi(data[i][4])
		if err != nil {
			loader.Fatal(err)
		}

		rating, err := strconv.ParseFloat(data[i][6], 32)
		if err != nil {
			loader.Fatal(err)
		}

		votes, err := strconv.Atoi(data[i][12])
		if err != nil {
			loader.Fatal(err)
		}

		series := &Series{
			PosterLink:  data[i][0],
			Title:       data[i][1],
			Year:        data[i][2],
			Certificate: data[i][3],
			Runtime:     runtime,
			Genre:       data[i][5],
			Rating:      float32(rating),
			Summary:     data[i][7],
			Cast: &CastList{
				Star1: data[i][8],
				Star2: data[i][9],
				Star3: data[i][10],
				Star4: data[i][11],
			},
			Votes: votes,
		}
		*serieslist = append(*serieslist, series)
	}
	return serieslist
}
