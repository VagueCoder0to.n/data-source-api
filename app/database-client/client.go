package database

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"math/rand"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/VagueCoder0to.n/Data-Source-API/app/database-client/loader"
)

// Wrapper is a wrapper structure around *loader.Loader with local objects
type Wrapper struct {
	*loader.Loader
	indexList  []string
	collection *mongo.Collection
}

// NewWrapper initiates and returns reference to Wrapper
func NewWrapper(ctx context.Context, logger *log.Logger) *Wrapper {
	wrapper := Wrapper{
		Loader:     loader.NewLoader(logger),
		indexList:  nil,
		collection: NewCollection(ctx, logger),
	}
	wrapper.LoadData(ctx, logger)
	return &wrapper
}

// NewCollection created and returns reference to mongo Collection
func NewCollection(ctx context.Context, logger *log.Logger) *mongo.Collection {
	rand.Seed(time.Now().UnixNano())
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://database:27017"))
	if err != nil {
		logger.Fatal(err)
	}
	return client.Database("Test").Collection("TestCollection")
}

// LoadData reads data from imdb_series_dataset.csv and inserts to Mongo Collection
// This should be invoked with initialization
func (w *Wrapper) LoadData(ctx context.Context, logger *log.Logger) {
	loader := loader.NewLoader(logger)
	s := loader.ReadData()

	for _, v := range *s {
		res, err := w.collection.InsertOne(ctx, v)
		if err != nil {
			logger.Fatal(err)
		}

		id, ok := res.InsertedID.(primitive.ObjectID)
		if !ok {
			logger.Fatal("Not Ok")
		}
		w.indexList = append(w.indexList, id.Hex())
	}
}

// FetchAndStream fetches random document from collection and streams to writer
func (w *Wrapper) FetchAndStream(ctx context.Context, writer io.Writer) {
	id := w.indexList[rand.Int()%len(w.indexList)]
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		w.Fatal(err)
	}

	doc := w.collection.FindOne(ctx, bson.M{"_id": objectID})
	series := w.NewSeries()
	doc.Decode(&series)

	err = json.NewEncoder(writer).Encode(&series)
	if err != nil {
		w.Fatal(err)
	}
}
