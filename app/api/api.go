package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/VagueCoder0to.n/Data-Source-API/app/api/handlers"
)

var (
	gopath string = os.Getenv("GOPATH")
	home   string = gopath + "/src/gitlab.com/VagueCoder0to.n/Data-Source-API"
	port   string
)

// Logger is type asserted with a log.Logger
type Logger struct{ *log.Logger }

// setPort Reads PORT's value from .env
func (logger *Logger) setPort() {
	viper.SetConfigFile(home + "/app/config/.env")
	err := viper.ReadInConfig()
	if err != nil {
		logger.Fatal(err)
	}

	pattern := regexp.MustCompile(`^[0-9]{0,4}$`)
	input := viper.Get("PORT")

	if input != nil {
		p := input.(string)
		if pattern.Match([]byte(p)) {
			port = p
		} else {
			logger.Fatal("Invalid PORT in .env")
		}
	} else {
		logger.Fatal("Set PORT in .env")
	}
}

// runServer runs the server in goroutine till interrupted
func (logger *Logger) runServer(s *http.Server) {
	logger.Fatal(s.ListenAndServe())
}

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	l := log.New(os.Stdout, "\r[Data-Source-API] ", log.Lshortfile|log.LstdFlags)
	logger := &Logger{l}
	logger.setPort()

	gh := handlers.NewGetHandler(ctx, logger.Logger)

	mux := http.NewServeMux()
	mux.Handle("/", gh)

	httpServer := &http.Server{
		Addr:         ":" + port,
		Handler:      mux,
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 20,
		IdleTimeout:  time.Second * 30,
	}

	logger.Printf("Serving on port: %v", port)
	go logger.runServer(httpServer)

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	signal.Notify(sig, os.Kill)

	recv := <-sig
	logger.Printf("Signal Received: %v\n", recv)
	httpServer.Shutdown(ctx)
}
