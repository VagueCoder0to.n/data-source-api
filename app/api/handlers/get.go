package handlers

import (
	"context"
	"log"
	"net/http"

	db "gitlab.com/VagueCoder0to.n/Data-Source-API/app/database-client"
)

// GetHandler is a handler object of the package
type GetHandler struct {
	wrapper *db.Wrapper
	logger  *log.Logger
}

// NewGetHandler initiates GetHandler object
func NewGetHandler(ctx context.Context, logger *log.Logger) *GetHandler {
	wrapper := db.NewWrapper(ctx, logger)
	return &GetHandler{wrapper, logger}
}

// ServerHTTP is the method for implementing Handler interface
func (h *GetHandler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	h.wrapper.FetchAndStream(r.Context(), rw)
}
