FROM alpine:3.12.3

RUN apk add --no-cache git make musl-dev go

RUN export GOROOT=/usr/lib/go
RUN export GOPATH=$HOME/go
RUN export GOBIN=$GOPATH/bin
RUN export PATH=$GOBIN:$PATH
RUN export WORKSPACE=$GOPATH/src/gitlab.com/VagueCoder0to.n/Data-Source-API/

ENV GOROOT /usr/lib/go
ENV GOPATH ${HOME}/go
ENV GOBIN $GOPATH/bin
ENV PATH $GOBIN:$PATH
ENV WORKSPACE $GOPATH/src/gitlab.com/VagueCoder0to.n/Data-Source-API/

RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin ${GOPATH}/pkg

WORKDIR ${WORKSPACE}

COPY . ${WORKSPACE}

RUN go get -v ./...

RUN go build -o Data-Source-API ./app/api

EXPOSE 8080

RUN mv Data-Source-API ${GOBIN}/

CMD ["Data-Source-API"]
