module gitlab.com/VagueCoder0to.n/Data-Source-API

go 1.13

require (
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.4.6
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
)
