build-binary:
	go build -o Data-Source-API ./app/api/

execution:
	make build-binary
	./Data-Source-API

docker-build:
	docker build -t vaguecoder/data-source-api:1.0.0 .

docker-run:
	docker network create data-source-api-network | true
	docker run -d --rm -p 27017:27017 --network data-source-api-network --name database mongo:4.4.2
	docker run -d --rm -p 8080:8080 --network data-source-api-network --name api vaguecoder/data-source-api:1.0.0

docker-stop:
	docker stop $$(docker ps -aq)
	docker network remove data-source-api-network | true
